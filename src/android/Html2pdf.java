package at.modalog.cordova.plugin.html2pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import android.R.bool;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.printservice.PrintJob;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.util.Log;
import org.apache.cordova.camera.FileHelper;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

@TargetApi(19)
public class Html2pdf extends CordovaPlugin
{
	private static final String LOG_TAG = "Html2Pdf";
	private CallbackContext callbackContext;

	// change your path on the sdcard here
	private String publicTmpDir = ".at.modalog.cordova.plugin.html2pdf"; // prepending a dot "." would make it hidden
	private String tmpPdfName = "print.pdf";

	// set to true to see the webview (useful for debugging)
    private final boolean showWebViewForDebugging = false;

	/**
	 * Constructor.
	 */
	public Html2pdf() {

	}

    @Override
    public boolean execute (String action, JSONArray args, CallbackContext callbackContext) throws JSONException
    {
		try
		{
			if( action.equals("create") )
			{

				if( args.getString(1) != null && args.getString(1) != "null" )
					this.tmpPdfName = args.getString(1);

				final Html2pdf self = this;
				final String imagePath = args.optString(0);
		        this.callbackContext = callbackContext;

		        cordova.getActivity().runOnUiThread( new Runnable() {
		            public void run()
					{
						String fileUri = "";
		            	try{
		            		Boolean landscape = false;

		            		BitmapFactory.Options options = new BitmapFactory.Options();
							options.inJustDecodeBounds = false;

							Bitmap unscaledBitmap = BitmapFactory.decodeStream(FileHelper.getInputStreamFromUriString(imagePath, cordova), null, options);
							Bitmap bitmap = null;

							if(unscaledBitmap.getWidth() > unscaledBitmap.getHeight()){
								landscape = true;
								bitmap = Bitmap.createScaledBitmap(unscaledBitmap, 800, 600, true);
							}else{
								bitmap = Bitmap.createScaledBitmap(unscaledBitmap, 600, 800, true);
							}

							String fileName = "ImageResized.png";

							File file = new File(Environment.getExternalStorageDirectory(), fileName);
							if(file.exists()) file.delete();
							try {
								FileOutputStream out = new FileOutputStream(file);
								bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
								out.flush();
								out.close();
							} catch(Exception e) {
								//TODO
							}
							String image = Uri.fromFile(file).toString();

							Document document = null;

							if (landscape){
								document = new Document(PageSize.A4.rotate(), 0, 0, 0, 0);
							} else{
								document = new Document(PageSize.A4, 0, 0, 0, 0);
							}

							fileUri = Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/" + tmpPdfName;
					        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileUri));
					        writer.setFullCompression();
					        document.open();
					        Image img = Image.getInstance(image);
					        if (landscape){
					        	img.scaleAbsolute(842,595);
					        }else{
					        	img.scaleAbsolute(595,842);
					        }
					        document.add(img);

					        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					        String date = df.format(Calendar.getInstance().getTime());
					        String keywords = "Referencia homologación AEAT: XXXXXXXXX, Fecha de digitalización: " + date + " Software: Digitalizado por MyTickets, Versión del Software: 1.0";

					        document.addKeywords(keywords);
					        document.close();
					        fileUri = Uri.fromFile(new File(fileUri)).toString();
						} catch(Exception e){
							// TODO
						}

				       	// send success result to cordova
		                PluginResult result = new PluginResult(PluginResult.Status.OK, fileUri);
		                result.setKeepCallback(false);
		                self.callbackContext.sendPluginResult(result);
		            }
		        });

		        // send "no-result" result to delay result handling
		        PluginResult pluginResult = new  PluginResult(PluginResult.Status.NO_RESULT);
		        pluginResult.setKeepCallback(true);
		        callbackContext.sendPluginResult(pluginResult);

				return true;
			}
			return false;
		}
		catch (JSONException e)
		{
			// TODO: signal JSON problem to JS
			//callbackContext.error("Problem with JSON");
			return false;
		}
    }


	/**
	 *
	 * Clean up and close all open files.
	 *
	 */
	@Override
	public void onDestroy()
	{
		// ToDo: clean up.
	}
}