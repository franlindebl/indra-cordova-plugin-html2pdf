/*
 * Copyright (C) 2014
 * Html 2 Pdf iOS Code: Clément Wehrung <cwehrung@nurves.com> (https://github.com/iclems/iOS-htmltopdf)
 * Cordova 3.3 Plugin & Html 2 Pdf Android Code: Modern Alchemists OG <office@modalog.at> (http://modalog.at)
 */

#import <Foundation/Foundation.h>

#import <Cordova/CDVPlugin.h>

#import "AppDelegate.h"


#define kImageSizeLandscape CGSizeMake(800,600)
#define kImageSizePortrait CGSizeMake(600,800)


@interface Html2pdf : CDVPlugin <UIWebViewDelegate, UIDocumentInteractionControllerDelegate>
{
}

// read / write
-(void) create: (CDVInvokedUrlCommand*)command;

@end
