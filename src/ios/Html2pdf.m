/*
 Copyright 2014 Modern Alchemists OG

 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

/*
 * Html 2 Pdf iOS Code: Clément Wehrung <cwehrung@nurves.com> (https://github.com/iclems/iOS-htmltopdf)
 */

#import "Html2pdf.h"

@interface Html2pdf (Private)

- (BOOL) saveHtml:(NSString*)html asPdf:(NSString*)filePath;

@end

@interface UIPrintPageRenderer (PDF)

- (NSData*) printToPDF;

@end

@implementation Html2pdf

- (void)create:(CDVInvokedUrlCommand*)command
{
    
    NSArray* arguments = command.arguments;

    NSLog(@"Creating pdf from html has been started.");
    
    NSString* pdfName = [arguments objectAtIndex:1];
    NSString* imagePath  = [[arguments objectAtIndex:0] stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    
    NSString *pdfPath=[self setupPDFDocumentNamed:pdfName];
    
    
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath ];
    
    if(image.size.width>image.size.height){
        [self resizeImageAtPath:imagePath size:kImageSizeLandscape];
        [self beginPDFPage:kImageSizeLandscape];
    }else{
        [self resizeImageAtPath:imagePath size:kImageSizePortrait];
        [self beginPDFPage:kImageSizePortrait];
    }

    
    UIImage *image2 = [UIImage imageWithContentsOfFile:imagePath ];

    
    [self addImage:image2 atPoint:CGPointMake(0.0, 0.0)];
    
    [self finishPDF];
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[NSString stringWithFormat:@"file://%@",pdfPath]] callbackId:command.callbackId];
}

- (NSString*)setupPDFDocumentNamed:(NSString*)name{
    
    NSString *newPDFName = name;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *pdfPath = [documentsDirectory stringByAppendingPathComponent:newPDFName];
    
    UIGraphicsBeginPDFContextToFile(pdfPath, CGRectZero, nil);
    return pdfPath;
}


- (void)beginPDFPage:(CGSize)size {
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, size.width, size.height), nil);
}

- (void)finishPDF {
    UIGraphicsEndPDFContext();
}

- (CGRect)addImage:(UIImage*)image atPoint:(CGPoint)point {
    CGRect imageFrame = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [image drawInRect:imageFrame];
    
    return imageFrame;
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    CGSize frameSize = newSize;
    
    //Get the image from the path
    UIImage* sourceImage = image;
    UIImage* newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = frameSize.width;
    CGFloat targetHeight = frameSize.height;
    CGFloat scaleFactor = 0.0;
    CGSize scaledSize = frameSize;
    
    // calculate the aspect ratio if the image is to large
    if (CGSizeEqualToSize(imageSize, frameSize) == NO) {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        // opposite comparison to imageByScalingAndCroppingForSize in order to contain the image within the given bounds
        if (widthFactor > heightFactor) {
            scaleFactor = heightFactor; // scale to fit height
        } else {
            scaleFactor = widthFactor; // scale to fit width
        }
        scaledSize = CGSizeMake(MIN(width * scaleFactor, targetWidth), MIN(height * scaleFactor, targetHeight));
    }
    
    // If the pixels are floats, it causes a white line in iOS8 and probably other versions too
    scaledSize.width = (int)scaledSize.width;
    scaledSize.height = (int)scaledSize.height;
    
    UIGraphicsBeginImageContext(scaledSize); // this will resize
    
    [sourceImage drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if (newImage == nil) {
        NSLog(@"could not scale image");
    }
    
    // pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;

}


-(void)resizeImageAtPath:(NSString*)path size:(CGSize)size{
    // get the arguments and the stuff inside of it
    
    CGSize frameSize = size;
    
    //Get the image from the path
    UIImage* sourceImage = [UIImage imageWithContentsOfFile:path];
    UIImage* newImage = [self imageWithImage:sourceImage scaledToSize:frameSize];
    
    // get the temp directory path
    // get the temp directory path

    NSError* err = nil;
    // generate unique file name
    NSData* data = UIImagePNGRepresentation(newImage);

    [data writeToFile:path options:NSAtomicWrite error:&err];
}


@end