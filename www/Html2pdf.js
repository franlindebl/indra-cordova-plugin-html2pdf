//////////////////////////////////////////
// Html2pdf.js
// Copyright (C) 2014 Modern Alchemits OG <office@modalog.at>
//
//////////////////////////////////////////
var exec = require('cordova/exec');

var Html2pdf =
{
    create : function( imagePath, filePath, success, error )
    {
    	exec(function(uri) {
    		success(uri);
		}, function(e) {
			error(e);
  		}, "Html2pdf", "create", [imagePath, filePath])
    }
}

module.exports = Html2pdf;
